import java.io.IOException;  
import java.util.Iterator;  
import java.util.StringTokenizer;
import java.util.Arrays;

import org.apache.hadoop.io.LongWritable; 
import org.apache.hadoop.conf.Configuration;  
import org.apache.hadoop.fs.Path;  
import org.apache.hadoop.io.IntWritable; 
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class IndexInverse {


    public static class Map extends Mapper<Object,Text,Text,Text>{
		private Text keyinfo = new Text();
		private Text valueinfo = new Text();
		
		public void map(Object key,Text value,Mapper<Object,Text,Text,Text>.Context context)
		throws IOException,InterruptedException{
			//分割单词
			StringTokenizer itr = new StringTokenizer(value.toString());
			//获取文件名
			FileSplit split = (FileSplit) context.getInputSplit();
			Path path = split.getPath();
			String pathName = path.getName();
			//遍历单词
			while(itr.hasMoreTokens()){
				keyinfo.set(itr.nextToken()+" "+pathName);
				valueinfo.set("1");
				context.write(keyinfo,valueinfo);
			}
		}
	}

    public static class Combiner extends Reducer<Text,Text,Text,Text>{
		private Text keyinfo = new Text();
		private Text valueinfo = new Text();
		
		public void reduce(Text key ,Iterable<Text> values,Reducer<Text,Text,Text,Text>.Context context)
		throws IOException,InterruptedException{
			String[] fields = key.toString().split(" ");//使用空格分割单词和文件名
			int sum =0;
			for(Text t:values){
				sum+=Integer.parseInt(t.toString());//将词频的String属性转为int
			}
			keyinfo.set(fields[0]);//fields[0]是单词，[1]则是分割的文件名
			valueinfo.set("< "+fields[1]+" : "+sum+" >");
			context.write(keyinfo,valueinfo);
		}
	}

   public static class Reduce extends Reducer<Text,Text,Text,Text>{
	   private Text valueinfo = new Text();
	   
	   public void reduce(Text key ,Iterable<Text> values,Reducer<Text,Text,Text,Text>.Context context)
	   throws IOException,InterruptedException{
		   String value ="";
		   for(Text t:values){
			   value +=t.toString()+",";
		   }
		   //排序文件名
		   String[] sortValue = value.split(",");
		   Arrays.sort(sortValue);
		   value="";
		   for(String str:sortValue){
			   value += str + "  ";
		   }
		   valueinfo.set(value);
		   context.write(key,valueinfo);
	   }
   }

    public static void main(String[] args) throws Exception  
    {  
        
		Configuration conf = new Configuration();
		
		Job job = Job.getInstance(conf);
		job.setJarByClass(IndexInverse.class); // 为job设置jar
		job.setMapperClass(Map.class);	//为job设置Mapper类  
		job.setCombinerClass(Combiner.class);	//为job设置Combiner类  
		job.setReducerClass(Reduce.class);	//为job设置Reduce类  
		job.setOutputKeyClass(Text.class);	//为reduce的输出数据设置Key类  
		job.setOutputValueClass(Text.class);	//为reduce输出设置value类  
		FileInputFormat.addInputPath(job, new Path(args[0]));  // addInputPaths():为map-reduce job设置路径数组作为输入列表 
        FileOutputFormat.setOutputPath(job, new Path(args[1])); // setInputPath()：为map-reduce job设置路径数组作为输出列表 
		System.exit(job.waitForCompletion(true)?0:1);

    }
}