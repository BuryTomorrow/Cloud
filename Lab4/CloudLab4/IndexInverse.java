/*
Map阶段  
<0,"Hello Hello world">
<1,"bye hadoop hadoop"> 
....  
  
context.write("Hello a.txt","1");
context.write("Hello a.txt","1");
context.write("world a.txt","1");
context.write("bye a.txt","1");
context.write("hadoop a.txt","1");
context.write("hadoop a.txt","1");
.... 
--------------------------------------------------------  
combiner阶段  
<"Hello a.txt","1">  
<"Hello a.txt","1">  
<"bye a.txt","1">
<"hadoop a.txt","1">  
<"hadoop a.txt","1">  
<"world a.txt","1">  
.... 
  
context.write("Hello","<a.txt,2>");  
context.write("bye","<a.txt,1>");
context.write("hadoop","<a.txt,2>");
context.write("world","<a.txt,1>");
....
--------------------------------------------------------  
Reducer阶段  
<"Hello",{"<a.txt,2>","<b.txt,1>"}>  
....
  
context.write("Hello","<a.txt,2> <b.txt,1>");
....
-------------------------------------------------------  
输出结果：
Hello	<a.txt,2> <b.txt,1> 
bye		<a.txt,1> <b.txt,2> 
hadoop	<a.txt,2> <b.txt,2> 
world	<a.txt,1> <b.txt,1>

*/



import java.io.IOException;  
import java.util.Iterator;  
import java.util.StringTokenizer;
import java.util.Arrays;

import org.apache.hadoop.io.LongWritable; 
import org.apache.hadoop.conf.Configuration;  
import org.apache.hadoop.fs.Path;  
import org.apache.hadoop.io.IntWritable; 
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class IndexInverse {
	
	 /** 
    * Mapper接口： 
    * WritableComparable接口：实现WritableComparable的类可以相互比较。所有被用作key的类应该实现此接口。 
    * Reporter 则可用于报告整个应用的运行进度，本例中未使用。  
    *  
    */ 
	
	public static class Map 
		extends Mapper<Object,Text,Text,Text> // 前两个为输入，后两个为输出
	{
		/** 
        * LongWritable, IntWritable, Text 均是 Hadoop 中实现的用于封装 Java 数据类型的类，这些类实现了WritableComparable接口， 
        * 都能够被串行化从而便于在分布式环境中进行数据交换，你可以将它们分别视为long,int,String 的替代品。 
        */ 
		private Text k = new Text(); // key 
		private Text v = new Text(); // value
		
		public void map(Object key, Text value,
				Mapper<Object, Text, Text, Text>.Context context)
				throws IOException,InterruptedException
		{
			// 得到整段文字
			String line = value.toString(); 
			// 分割单词
			StringTokenizer tokenizer = new StringTokenizer(line);
			// 得到来源文件名
			FileSplit inputSplit = (FileSplit) context.getInputSplit();
			Path path = inputSplit.getPath();
			String pathName = path.getName();
			// 遍历所有单词
			while (tokenizer.hasMoreTokens())
			{
				k.set(tokenizer.nextToken() + " "  + pathName);
				v.set("1");
				context.write(k,v);
			}
		}
		
	}
	// 合并的过程
	public static class Combiner 
		extends Reducer<Text,Text,Text,Text>
	{
		private Text k = new Text();
		private Text v = new Text();
		
		public void reduce(Text key, Iterable<Text> values,
				Reducer<Text,Text,Text,Text>.Context context)
				throws IOException, InterruptedException
		{
			String[] fields = key.toString().split(" "); // 通过空格分割单词（前面map输出的key）
			int sum = 0 ;
			for(Text t : values){
				sum += Integer.parseInt(t.toString()); // 将String转为int
			}
			k.set(fields[0]); // fields[0]是单词；fields[1]是单词所在的文件名
			v.set("<" + fields[1] + "," + sum + ">");
			context.write(k,v);
		}
	}
	
	
	
	public static class Reduce 
		extends Reducer<Text, Text, Text, Text>
	{
		private Text v = new Text();
		
		public void reduce(Text key, Iterable<Text> values, 
				Reducer<Text, Text, Text, Text>.Context context)
				throws IOException,InterruptedException
		{
			String value="";
			for(Text t: values){
				value += t.toString() + " ";
			}
			// sort一下后面的文件名排序
			String[] sortValue = value.split(" ");
			Arrays.sort(sortValue);
			value="";
			for(String str : sortValue){
				value += str + " ";
			}
			
			v.set(value);
			context.write(key,v);
		}
	}
	public static void main(String[] args) throws Exception  
    {  
        
		Configuration conf = new Configuration();
		
		Job job = Job.getInstance(conf);
		job.setJarByClass(IndexInverse.class); // 为job设置jar
		job.setMapperClass(Map.class);	//为job设置Mapper类  
		job.setCombinerClass(Combiner.class);	//为job设置Combiner类  
		job.setReducerClass(Reduce.class);	//为job设置Reduce类  
		job.setOutputKeyClass(Text.class);	//为reduce的输出数据设置Key类  
		job.setOutputValueClass(Text.class);	//为reduce输出设置value类  
		FileInputFormat.addInputPath(job, new Path(args[0]));  // addInputPaths():为map-reduce job设置路径数组作为输入列表 
        FileOutputFormat.setOutputPath(job, new Path(args[1])); // setInputPath()：为map-reduce job设置路径数组作为输出列表 
		System.exit(job.waitForCompletion(true)?0:1);
		
		
    }
	
}


