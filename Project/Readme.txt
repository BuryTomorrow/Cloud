朴素贝叶斯实现

使用hadoop框架实现朴素贝叶斯的二分类算法，并验证预测结果

相关文件：
NaiveBayes.java			---- 主要运行代码，在虚拟机中编译打包后按照lab3的命令执行
				     即/usr/local/hadoop/bin/hadoop jar NaiveBayes.jar NaiveBayes /input /output
train.txt			---- 训练集，将其放入HDFS上的input文件夹当中

val.txt				---- 测试集，将其放入HDFS上的user下的"用户"目录下

输出结果的output文件在中结果形式：

  真实数据标签	    预测数据标签

