import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.io.IOException;  
import java.util.Iterator;  
import java.util.StringTokenizer;
import java.util.Arrays;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.hadoop.io.LongWritable; 
import org.apache.hadoop.conf.Configuration;  
import org.apache.hadoop.fs.Path;  
import org.apache.hadoop.io.IntWritable; 
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable; 
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;

import org.apache.hadoop.mapreduce.Counter;

public class NaiveBayes{
	//Mapper
	public static class NBMapper extends Mapper<Object, Text, IntWritable, NBWritable> {  
	    private IntWritable myKey = new IntWritable();  
	    private NBWritable myValue = new NBWritable();//输出类型 
	    @Override  
	    protected void map(Object key, Text value, Context context)  
	            throws IOException, InterruptedException {  
	        //获取输入的文本
	        String[] trains = value.toString().split(",");//以逗号为分隔符获取文本
	        int[] values = new int[trains.length];
	        for(int i=0; i < trains.length;i++){  
	            values[i] = Integer.parseInt(trains[i]);  
	        }
	        int label = values[0];  //存放类别,数组的第一个是标签
	        int[] attribute = new int[values.length-1]; //存放文本的各个属性
	        for(int i =1;i<values.length;i++){  
	            attribute[i-1] = values[i];  
	        }  
	        myKey.set(label);  
	        myValue.setValue(attribute);  
	        context.write(myKey, myValue);  //设置键值对,以标签作为键，以属性作为值
	          
	    }  
	}  
	//Writable，实现数据的存储
	public static class NBWritable implements Writable{  
	    private int[] value;  
	    public NBWritable() { }  
	    public NBWritable(int[] value){  //实现构造函数
	        this.setValue(value);  
	    }  
	    @Override  
	    public void write(DataOutput out) throws IOException {  
	        out.writeInt(value.length);  
	        for(int i=0; i<value.length;i++){  
	            out.writeInt(value[i]);  
	        }  
	    }  
	  
	    @Override  
	    public void readFields(DataInput in) throws IOException {  //通过readFields方法从序列化的数据流中读出进行赋值
	        int vLength = in.readInt();  
	        value = new int[vLength];  
	        for(int i=0; i<vLength;i++){  
	            value[i] = in.readInt();  
	        }  
	    }  
	    public int[] getValue() {  
	        return value;  
	    }  
	    public void setValue(int[] value) {  
	        this.value = value;  
	    }  
	}  
	//Reducer,分别计算相同标签下的概率
	public static class NBReducer extends Reducer<IntWritable, NBWritable, IntWritable, IntWritable>{  
	    private String testFilePath;  
	    // 存储测试集  
	    private ArrayList<int[]> testData = new ArrayList<>();  
	    // 保存相同标签的所有数据，所以共两个集合  
	    private ArrayList<AllRate> allData = new ArrayList<>();  
	  	// 在reduce之前需要读取测试集的数据，读取方式通过制定文件目录读取
	    @Override  
	    protected void setup(Context context)  
	            throws IOException, InterruptedException,NumberFormatException {  
	        Configuration conf = context.getConfiguration();  
	        testFilePath = conf.get("TestFilePath");  
	        Path path = new Path(testFilePath);  
	        FileSystem fs = path.getFileSystem(conf);  
	        //获取测试集文本路径，开始读取测试集数据
	        FSDataInputStream data = fs.open(path);  
	        BufferedReader bf = new BufferedReader(new InputStreamReader(data));  
	        String line = "";  
	        while ((line = bf.readLine()) != null) { //每行同样以逗号作为分隔符读取数据  
	            String[] str = line.split(",");  
	            int[] myData = new int[str.length];  
	            for(int i=0;i<str.length;i++){  
	                myData[i] = Integer.parseInt(str[i]);  
	            }  
	            testData.add(myData);  //testData中存储了包括标签和属性，其中数组第一位是标签
	        }  
	        bf.close();  
	        data.close();
	    }  
	    //计算概率,输入的是相同的键值的所有样本集
	    @Override  
	    protected void reduce(IntWritable key, Iterable<NBWritable> values,Context context)  
	            throws IOException, InterruptedException {
	        //初始化保存各个相同类别当中各个属性的属性值为1的样本数，为了避免0的出现，使用拉普拉斯平滑，初始其为1
	        Double[] attributeCount = new Double[testData.get(0).length-1];  
	        for(int i=0;i<attributeCount.length;i++){  
	            attributeCount[i] = 1.0;  
	        }
	        Long sum = 2L;//统计相同键值的总样本数
	        // 计算每个类别中，每个属性值为1的个数  
	        for (NBWritable nbWritable : values) {
	            int[] MyAttribute = nbWritable.getValue();  
	            for(int i=0; i < MyAttribute.length;i++){  
	                attributeCount[i] += MyAttribute[i]; //由于特征属性只有0和1两种，所以可以通过加和来完成统计 
	            }
	            sum += 1;  
	        }
	        for(int i=0;i<attributeCount.length;i++){  
	            attributeCount[i] = attributeCount[i]/sum;  //得到属性的似然度，朴素贝叶斯假设各个属性间独立
	        }  
	        allData.add(new AllRate(sum,attributeCount,key.get())); //保留相同标签的总样本数，各个属性的概率值，以及其标签
	    }  

	    //在reduce之后执行会执行cleanup函数，通过重写来计算最终测试集判别的概率
	    private IntWritable myKey = new IntWritable();  
	    private IntWritable myValue = new IntWritable();  
	    @Override  
	    protected void cleanup(Context context) throws IOException, InterruptedException {  
	        //用哈希表作为存储最后的两个标签的结果
	        HashMap<Integer, Double> labelG = new HashMap<>();
	        //计算训练数据的总数 
	        Long allSum = 0L;
	        for (AllRate allrate : allData) { 
	            allSum += allrate.getSum();  
	        }
	        //以标签作为键值并初始化结果为先验概率，也就是该标签的样本数除以总训练样本数
	        for(int i=0; i<allData.size();i++){  
	            labelG.put(allData.get(i).getType(),Double.parseDouble(allData.get(i).getSum().toString())/allSum);  
	        }
	        //样本预测
	        int sum = 0; //统计测试集样本总数，用于计算正确率 
	        int correct = 0;  //统计预测正确的样本数
	        for(int[] test: testData){  
	            int predict = getClasify(context,sum,test, labelG); //通过朴素贝叶斯来预测结果 
	            if(test[0] == predict){ //如果真实标签与预测的结果相同，则正确
	                correct += 1;  
	            }
	            sum +=1;  
	            myKey.set(test[0]);  
	            myValue.set(predict);  
	            context.write(myKey, myValue); //输出真实的标签以及预测的标签
	        }
	        Counter countPrint1 = context.getCounter("预测的正确率为:", "  "+(double)correct/sum+"  ;");
			countPrint1.increment(1l);
	    }  
	    //计算某个样本的分类结果
	    private int getClasify(Context context,int index,int[] test,HashMap<Integer, Double> labelG ) {  
	        double[] result = new double[allData.size()]; //以类别的长度作为数组的长度  
	        for(int i = 0; i<allData.size();i++){ 
	            double count = 0.0;  
	            AllRate attributeRate = allData.get(i);  
	            Double[] pdata = attributeRate.getValue();
	            //为了避免精度损失问题，则使用log对数来处理，所以为求和的方式 
	            //测试文本下标从1开始，因为第0位是标签
	            for(int j=1;j<test.length;j++){ 
	                if(test[j] == 1){  
	                    // 如果该测试文本的属性为1，那么就直接求得和 
	                    count += Math.log(pdata[j-1]);  
	                }else{  
	                	//如果该测试文本的属性为0，那就求1-p再求概率和，因为存储的是为1的概率
	                    count += Math.log(1- pdata[j-1]);  
	                } 
	            }
	            count += Math.log(labelG.get(attributeRate.getType()));//最后加上先验概率
	            result[i] = count;  
	        }
	        // 求出最大的概率,如果为result[0]大的话，那么预测的标签值就为存储的allData0号位的数据的标签
	       	int predict = 0;
	        if(result[0] > result[1]){  
	            predict = allData.get(0).getType();  
	        }else{  
	            predict = allData.get(1).getType();  
	        }
	        Counter countPrint1 = context.getCounter("对样本的预测结果", "第 "+index+" 样本的真实为: "+test[0]+" 预测为: "+
	        	predict+" 0的概率是: "+result[0]+"  1的概率是："+result[1]+" ;\n");
			countPrint1.increment(1l);
	        return predict;
	    }  
	}  
	//存储相同标签下各个属性的概率值
	public static class AllRate {  
	    private Long sum;  
	    private Double[] value;  
	    private int type;  
	    public AllRate(){}  
	    public AllRate(Long sum, Double[] value,int type){  
	        this.sum = sum;  
	        this.value = value;  
	        this.type = type;  
	    }
	    //获取该标签下所有样本的特征
	    public Double[] getValue() {  
	        return value;  
	    }
	    //获取该标签下的样本总数
	    public Long getSum() {  
	        return sum;  
	    }
	    //获取其标签
	    public int getType() {  
	        return type;  
	    } 
	}
	public static void main(String[] args) throws Exception  {  
		Configuration conf = new Configuration();
		//设置测试集读取路径
		conf.set("TestFilePath","val.txt");
		Job job = Job.getInstance(conf);
		job.setJarByClass(NaiveBayes.class); // 为job设置jar
		job.setMapperClass(NBMapper.class);	//为job设置Mapper类    
		job.setMapOutputValueClass(NBWritable.class);//为map的输出数据设置value类 
		job.setMapOutputKeyClass(IntWritable.class);//为map的输出数据设置key类 
		job.setReducerClass(NBReducer.class);	//为job设置Reduce类  	
		job.setOutputKeyClass(IntWritable.class);	//为reduce的输出数据设置Key类  
		job.setOutputValueClass(IntWritable.class);	//为reduce输出设置value类
		FileInputFormat.addInputPath(job, new Path(args[0]));  // addInputPaths():为map-reduce job设置路径数组作为输入列表 
        FileOutputFormat.setOutputPath(job, new Path(args[1])); // setInputPath()：为map-reduce job设置路径数组作为输出列表 
		System.exit(job.waitForCompletion(true)?0:1);
    }
}